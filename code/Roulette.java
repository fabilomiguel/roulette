import java.util.Scanner;

public class Roulette 
{
    private int userMoney;

    public Roulette() 
    {
        userMoney = 1000;
    }

    public int getUserBetValue() 
    {
        Scanner scanner = new Scanner(System.in);
        while (true) 
        {
            System.out.println("Your current balance: $" + userMoney);
            System.out.print("Place your bet (0 to quit): ");
            int betValue = scanner.nextInt();
            if (betValue == 0) 
            {
                return -1;
            } 
            else if (betValue <= userMoney) 
            {
                return betValue;
            } 
            else 
            {
                System.out.println("You don't have enough money for that bet.");
            }
        }
    }

    public int getUserBetNumber()
    {
        System.out.print("Choose a number to bet on (0-36): ");
        Scanner scanner = new Scanner(System.in);
        int numThatWasBet= scanner.nextInt();
        if (numThatWasBet <=36 && numThatWasBet >0)
        {
            return numThatWasBet;
        }
        else
        {
            return -1;
        }
    }

    public boolean checkWin(RouletteWheel wheel, int numThatWasBet) {
        int wheelValue = wheel.getValue();
        return numThatWasBet == wheelValue;
    }

    public void play() {
        RouletteWheel wheel = new RouletteWheel();

        while (userMoney > 0) {
            int betValue = getUserBetValue();
            int numThatWasBet = getUserBetNumber();
            if (betValue == -1) {
                System.out.println("Thanks for playing!");
                break;
            }

            wheel.spin();
            boolean won = checkWin(wheel, numThatWasBet);

            if (won) {
                int winAmount = betValue * 35;
                userMoney += winAmount;
                System.out.println("Congratulations! You won $" + winAmount + "!");
            } else {
                userMoney -= betValue;
                System.out.println("Sorry, you lost.");
            }

            System.out.println("Roulette spun: " + wheel.getValue());
            System.out.println("Your balance: $" + userMoney + "\n");
        }

        System.out.println("Game over.");
    }

    public static void main(String[] args) {
        Roulette game = new Roulette();
        game.play();
    }
}
